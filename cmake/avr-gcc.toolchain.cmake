##########################################################################
# The toolchain requires some variables set.
#
# AVR_MCU (default: atmega8)
#	 the type of AVR the application is built for
# AVR_L_FUSE (NO DEFAULT)
#	 the LOW fuse value for the MCU used
# AVR_H_FUSE (NO DEFAULT)
#	 the HIGH fuse value for the MCU used
# AVR_UPLOADTOOL (default: avrdude)
#	 the application used to upload to the MCU
# AVR_UPLOADTOOL_PORT (default: usb)
#	 the port used for the upload tool, e.g. usb
# AVR_PROGRAMMER (default: usbasp)
#	 the programmer hardware used, e.g. usbasp
##########################################################################

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR avr)
SET(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_CROSSCOMPILING 1)

find_path(TOOLCHAIN_ROOT
	NAMES
		avr-gcc
	PATHS
		/usr/bin
		/usr/local/bin
		/bin
		$ENV{AVR_ROOT}
)
if(NOT TOOLCHAIN_ROOT)
	message(FATAL_ERROR "Toolchain root could not be found!!!")
endif(NOT TOOLCHAIN_ROOT)

set(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_ROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_C_COMPILER   "${TOOLCHAIN_ROOT}/avr-gcc${OS_SUFFIX}"     CACHE PATH "gcc"     FORCE)
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_ROOT}/avr-g++${OS_SUFFIX}"     CACHE PATH "g++"     FORCE)
set(CMAKE_AR           "${TOOLCHAIN_ROOT}/avr-ar${OS_SUFFIX}"      CACHE PATH "ar"      FORCE)
set(CMAKE_LINKER       "${TOOLCHAIN_ROOT}/avr-ld${OS_SUFFIX}"      CACHE PATH "linker"  FORCE)
set(CMAKE_NM           "${TOOLCHAIN_ROOT}/avr-nm${OS_SUFFIX}"      CACHE PATH "nm"      FORCE)
set(CMAKE_OBJCOPY      "${TOOLCHAIN_ROOT}/avr-objcopy${OS_SUFFIX}" CACHE PATH "objcopy" FORCE)
set(CMAKE_OBJDUMP      "${TOOLCHAIN_ROOT}/avr-objdump${OS_SUFFIX}" CACHE PATH "objdump" FORCE)
set(CMAKE_STRIP        "${TOOLCHAIN_ROOT}/avr-strip${OS_SUFFIX}"   CACHE PATH "strip"   FORCE)
set(CMAKE_RANLIB       "${TOOLCHAIN_ROOT}/avr-ranlib${OS_SUFFIX}"  CACHE PATH "ranlib"  FORCE)
set(AVR_SIZE           "${TOOLCHAIN_ROOT}/avr-size${OS_SUFFIX}"	   CACHE PATH "size"    FORCE)

set(AVR_LINKER_LIBS "-lc -lm -lgcc")

# some necessary tools and variables for AVR builds, which may not be defined yet
# - AVR_UPLOADTOOL
# - AVR_UPLOADTOOL_PORT
# - AVR_UPLOADTOOL_BITCLOCK
# - AVR_PROGRAMMER
# - AVR_MCU
# - AVR_SIZE_ARGS

# default upload tool
if(NOT AVR_UPLOADTOOL)
	set(AVR_UPLOADTOOL avrdude CACHE STRING "Set default upload tool: avrdude" )
	find_program(AVR_UPLOADTOOL avrdude)
endif(NOT AVR_UPLOADTOOL)

# default upload tool port
if(NOT AVR_UPLOADTOOL_PORT)
	set(AVR_UPLOADTOOL_PORT usb CACHE STRING "Set default upload tool port: usb")
endif(NOT AVR_UPLOADTOOL_PORT)

# default upload tool bitclock
if(NOT AVR_UPLOADTOOL_BITCLOCK)
	set(AVR_UPLOADTOOL_BITCLOCK 10 CACHE STRING "Set default upload tool bitclock: 10" )
endif(NOT AVR_UPLOADTOOL_BITCLOCK)

# default programmer (hardware)
if(NOT AVR_PROGRAMMER)
	set(AVR_PROGRAMMER usbasp CACHE STRING "Set default programmer hardware model: usbasp")
endif(NOT AVR_PROGRAMMER)

# default MCU (chip)
if(NOT AVR_MCU)
	set(AVR_MCU atmega8 CACHE STRING "Set default MCU: atmega8 (see 'avr-gcc --target-help' for valid values)")
endif(NOT AVR_MCU)
