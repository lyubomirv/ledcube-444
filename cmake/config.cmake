set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99 -fshort-enums -Wall -Werror -pedantic -pedantic-errors")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fshort-enums -Wall -Werror -pedantic -pedantic-errors")

set(CMAKE_C_FLAGS_RELEASE "-Os")
set(CMAKE_CXX_FLAGS_RELEASE "-Os")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_C_FLAGS_DEBUG "-O0 -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -save-temps -g -gdwarf-3 -gstrict-dwarf")

if(NOT ((CMAKE_BUILD_TYPE MATCHES Release) OR
		  (CMAKE_BUILD_TYPE MATCHES RelWithDebInfo) OR
		  (CMAKE_BUILD_TYPE MATCHES Debug) OR
		  (CMAKE_BUILD_TYPE MATCHES MinSizeRel)))
	set(
		CMAKE_BUILD_TYPE Release
		CACHE STRING "Choose cmake build type: Debug Release RelWithDebInfo MinSizeRel"
		FORCE
	)
endif()

message(STATUS "Upload tool: ${AVR_UPLOADTOOL}")
message(STATUS "Programmer: ${AVR_PROGRAMMER}")
message(STATUS "Upload port: ${AVR_UPLOADTOOL_PORT}")
message(STATUS "Upload options: ${AVR_UPLOADTOOL_BITCLOCK}")
message(STATUS "µMCU: ${AVR_MCU}")
message(STATUS "H_FUSE: ${AVR_H_FUSE}")
message(STATUS "L_FUSE: ${AVR_L_FUSE}")
