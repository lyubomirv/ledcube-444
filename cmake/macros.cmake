##########################################################################
# add_avr_executable
# - TARGET_NAME
function(add_avr_executable TARGET_NAME)
	if(NOT ARGN)
		message(FATAL_ERROR "No source files given for ${TARGET_NAME}.")
	endif()
	
	# define file names we will be using
	set(ELF_FILE ${TARGET_NAME}-${AVR_MCU}.elf)
	set(HEX_FILE ${TARGET_NAME}-${AVR_MCU}.hex)
	set(LST_FILE ${TARGET_NAME}-${AVR_MCU}.lst)
	set(EEPROM_IMAGE ${TARGET_NAME}-${AVR_MCU}-eeprom.hex)

	# add elf target
	add_executable(${ELF_FILE}
		${ARGN})

	# set compile and link flags for elf target
	set_target_properties(
		${ELF_FILE}
		PROPERTIES
			COMPILE_FLAGS "-mmcu=${AVR_MCU}"
			LINK_FLAGS	 "-mmcu=${AVR_MCU} ${AVR_LINKER_LIBS}")
	
	# generate the lst file
	add_custom_command(
		OUTPUT ${LST_FILE}
		COMMAND
			${CMAKE_OBJDUMP} -h -S ${ELF_FILE} > ${LST_FILE}
		DEPENDS ${ELF_FILE})

	# create hex file
	add_custom_command(
		OUTPUT ${HEX_FILE}
		COMMAND
			${CMAKE_OBJCOPY} -j .text -j .data -O ihex ${ELF_FILE} ${HEX_FILE}
		DEPENDS ${ELF_FILE})
	
	add_custom_command(
		OUTPUT "print-size-${ELF_FILE}"
		COMMAND
			${AVR_SIZE} -C;--mcu=${AVR_MCU} ${ELF_FILE}
		DEPENDS ${ELF_FILE})
	
	if(APPLE)
		set(AVR_SIZE_ARGS -B)
	else()
		set(AVR_SIZE_ARGS -C;--mcu=${AVR_MCU})
	endif()
	# build the intel hex file for the device
	add_custom_target(
		${TARGET_NAME}
		ALL
		DEPENDS ${HEX_FILE} ${LST_FILE} "print-size-${ELF_FILE}")

	set_target_properties(
		${TARGET_NAME}
		PROPERTIES
			OUTPUT_NAME ${ELF_FILE})
	
	# upload - with avrdude
	add_custom_target(
		upload_${TARGET_NAME}
		${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -B ${AVR_UPLOADTOOL_BITCLOCK}
			-U flash:w:${HEX_FILE}
			-P ${AVR_UPLOADTOOL_PORT}
		DEPENDS ${HEX_FILE}
		COMMENT "Uploading ${HEX_FILE} to ${AVR_MCU} using ${AVR_PROGRAMMER}.")

	# upload eeprom only - with avrdude
	# see also bug http://savannah.nongnu.org/bugs/?40142
	add_custom_target(
		upload_eeprom
		${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -B ${AVR_UPLOADTOOL_BITCLOCK}
			-U eeprom:w:${EEPROM_IMAGE}
			-P ${AVR_UPLOADTOOL_PORT}
		DEPENDS ${EEPROM_IMAGE}
		COMMENT "Uploading ${EEPROM_IMAGE} to ${AVR_MCU} using ${AVR_PROGRAMMER}.")

	# get status
	add_custom_target(
		get_status
		${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -P ${AVR_UPLOADTOOL_PORT} -B ${AVR_UPLOADTOOL_BITCLOCK} -n -v
		COMMENT "Get status from ${AVR_MCU}.")

	# get fuses
	add_custom_target(
		get_fuses
		${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -P ${AVR_UPLOADTOOL_PORT} -B ${AVR_UPLOADTOOL_BITCLOCK} -n
			-U lfuse:r:-:b
			-U hfuse:r:-:b
		COMMENT "Get fuses from ${AVR_MCU}.")

	if(AVR_H_FUSE AND AVR_L_FUSE)
		# set fuses
		add_custom_target(
			set_fuses
			${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -P ${AVR_UPLOADTOOL_PORT} -B ${AVR_UPLOADTOOL_BITCLOCK}
				-U lfuse:w:${AVR_L_FUSE}:m
				-U hfuse:w:${AVR_H_FUSE}:m
				COMMENT "Setup: High Fuse: ${AVR_H_FUSE} Low Fuse: ${AVR_L_FUSE}.")
	endif()

	# get oscillator calibration
	add_custom_target(
		get_calibration
			${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -P ${AVR_UPLOADTOOL_PORT} -B ${AVR_UPLOADTOOL_BITCLOCK}
			-U calibration:r:${AVR_MCU}_calib.tmp:r
			COMMENT "Write calibration status of internal oscillator to ${AVR_MCU}_calib.tmp."
	)

	# set oscillator calibration
	add_custom_target(
		set_calibration
		${AVR_UPLOADTOOL} -p ${AVR_MCU} -c ${AVR_PROGRAMMER} -P ${AVR_UPLOADTOOL_PORT} -B ${AVR_UPLOADTOOL_BITCLOCK}
			-U calibration:w:${AVR_MCU}_calib.hex
			COMMENT "Program calibration status of internal oscillator from ${AVR_MCU}_calib.hex."
	)

	# disassemble
	add_custom_target(
		disassemble_${TARGET_NAME}
		${CMAKE_OBJDUMP} -h -S ${ELF_FILE} > ${TARGET_NAME}.lst
		DEPENDS ${ELF_FILE}
)
endfunction(add_avr_executable)

##########################################################################
# add_avr_library
# - LIBRARY_NAME
function(add_avr_library LIBRARY_NAME)
	if(NOT ARGN)
		message(FATAL_ERROR "No source files given for ${LIBRARY_NAME}.")
	endif()

	set(LIB_FILE ${LIBRARY_NAME}${MCU_TYPE_FOR_FILENAME})

	add_library(${LIB_FILE} STATIC ${ARGN})

	set_target_properties(
		${LIB_FILE}
		PROPERTIES
			COMPILE_FLAGS "-mmcu=${AVR_MCU}"
			OUTPUT_NAME "${LIB_FILE}"
	)

	if(NOT TARGET ${LIBRARY_NAME})
		add_custom_target(
			${LIBRARY_NAME}
			ALL
			DEPENDS ${LIB_FILE}
		)

		set_target_properties(
			${LIBRARY_NAME}
			PROPERTIES
				OUTPUT_NAME "${LIB_FILE}"
		)
	endif(NOT TARGET ${LIBRARY_NAME})

endfunction(add_avr_library)

##########################################################################
# avr_target_link_libraries
# - TARGET_NAME
function(avr_target_link_libraries TARGET_NAME)
	if(NOT ARGN)
		message(FATAL_ERROR "Nothing to link to ${TARGET_NAME}.")
	endif()

	get_target_property(TARGET_LIST ${TARGET_NAME} OUTPUT_NAME)

	foreach(TGT ${ARGN})
		if(TARGET ${TGT})
			get_target_property(ARG_NAME ${TGT} OUTPUT_NAME)
			list(APPEND TARGET_LIST ${ARG_NAME})
		else(TARGET ${TGT})
			list(APPEND NON_TARGET_LIST ${TGT})
		endif(TARGET ${TGT})
	endforeach(TGT ${ARGN})

	target_link_libraries(${TARGET_LIST} ${NON_TARGET_LIST})
endfunction(avr_target_link_libraries)
