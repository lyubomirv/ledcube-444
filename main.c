#include <assert.h>
#include <stdint.h>
#include <string.h>

#include <avr/eeprom.h>
#include <avr/io.h>
#include <avr/iom48pa.h>
#include <avr/interrupt.h>
#include <util/delay.h>

// Помощни функции.

/**
 * @brief Задава стойност на краче от порт B.
 * @param pin номер на краче от порт B
 * @param value стойност: 0 или 1
 */
void setPinB(uint8_t pin, uint8_t value)
{
	PORTB = value
		? PORTB | _BV(pin)
		: PORTB & ~_BV(pin);
}

/**
 * @brief Задава стойност на краче от порт C.
 * @param pin номер на краче от порт C
 * @param value стойност: 0 или 1
 */
void setPinC(uint8_t pin, uint8_t value)
{
	PORTC = value
		? PORTC | _BV(pin)
		: PORTC & ~_BV(pin);
}

/**
 * @brief Задава стойност на краче от порт D.
 * @param pin номер на краче от порт D
 * @param value стойност: 0 или 1
 */
void setPinD(uint8_t pin, uint8_t value)
{
	PORTD = value
		? PORTD | _BV(pin)
		: PORTD & ~_BV(pin);
}

// Анулиране на подскачането.
#define PORTC_BUTTONS_COUNT 5				// Брой бутони в порт C. Или по-точно
											// максимален номер на краче в порта,
											// на което има бутон.
#define PORTC_BUTTONS_DEBOUNCE_VALUE 1		// Стойноста за анулиране на подскачането.
											// Може да трябва да се настрои.

/**
 * @brief Проверява дали има натиснати бутони в порт C.
 *
 * За всеки бутон връща 1 в момента на натискането, като анулира подскачането.
 * Ако след това функцията бъде извикана отново, ще върне 0 за същия бутон,
 * дори ако той още е натиснат, тъй като отчита само първоначалното натискане.
 * Бутоните трябва да са свързани така, че при натискане да правят връзка с 0V.
 * На крачетата на бутоните трябва да е включен вътрешния резистор.
 * @return 8-битово число, в което всеки бит отразява състоянието на бутон свързан
 * със съответния номер краче.
 */
uint8_t isPressedC()
{
	static uint8_t pressed = 0;
	static uint8_t counters[PORTC_BUTTONS_COUNT] = { 0 };

	uint8_t current = 0;
	for(uint8_t i = 0; i < PORTC_BUTTONS_COUNT; ++i)
	{
		if(bit_is_set(DDRC, i))
		{
			// Крачето е за изход.
			continue;
		}

		if(!(pressed & (1 << i)))
		{
			if(bit_is_clear(PINC, i))
			{
				++counters[i];
			}
			else
			{
				counters[i] = 0;
			}
		}
		else
		{
			if(bit_is_set(PINC, i))
			{
				++counters[i];
			}
			else
			{
				counters[i] = 0;
			}
		}

		if(counters[i] > PORTC_BUTTONS_DEBOUNCE_VALUE)
		{
			pressed ^= 1 << i;
			counters[i] = 0;

			if(pressed & (1 << i))
			{
				current ^= 1 << i;
			}
		}
	}

	return current;
}





// Глобални
#define BUTTON_PIN PINC4

// Всеки 2 байта в този масив отговарят на едно ниво в кубчето.
// Първият байт е за порт B, вторият за порт D.
// 0-1 - най-долно ниво, ... , 6-7 - най-горно ниво.
uint8_t LED[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

// Когато е 1, режимът трябва да бъде сменен. Всеки режим трябва да проверява
// този флаг в цикъла си, за да знае кога да приключи.
// Този флаг трябва да се връща на 0 ръчно от главния цикъл.
uint8_t ShouldChangeModeFlag = 0;

void clear()
{
	memset(LED, 0, sizeof(LED) / sizeof(LED[0]));
}

/**
 * @brief Задава стойността (вкл/изкл) на светодиода с дадените координати.
 * @param x координата X – от ляво надясно
 * @param y координата Y – отпред назад
 * @param z координата Z – от долу нагоре
 * @param value вкл/изкл – 0/1
 */
void setLED(uint8_t x, uint8_t y, uint8_t z, uint8_t value)
{
	if(x > 3 || y > 3 || z > 3 || value > 1)
	{
		return;
	}

	uint8_t* byte = LED + (z * 2) + (y > 1 ? 1 : 0);
	if(value)
	{
		*byte |= 1 << (x + ((y % 2) * 4));
	}
	else
	{
		*byte &= ~(1 << (x + ((y % 2) * 4)));
	}
}

/**
 * @brief Проверява стойността (вкл/изкл) на светодиода с дадените координати.
 * @param x координата X – от ляво надясно
 * @param y координата Y – отпред назад
 * @param z координата Z – от долу нагоре
 */
uint8_t getLED(uint8_t x, uint8_t y, uint8_t z)
{
	if(x > 3 || y > 3 || z > 3)
	{
		return 0;
	}

	uint8_t* byte = LED + (z * 2) + (y > 1 ? 1 : 0);
	return (*byte & (1 << (x + ((y % 2) * 4)))) > 0 ? 1 : 0;
}

/**
 * @brief Показва дали трябва да се смени режимът.
 *
 * Брояч вика тази функция, и ако тя върне 1, задава стойност 1 в ShouldChangeModeFlag.
 * @return 1 ако режимът трябва да се смени, иначе 0.
 */
uint8_t shouldChangeMode()
{
	return isPressedC() & (1 << BUTTON_PIN);
}






/**
 * @brief Смяна на лампичката с натискане на бутон.
 */
void changeWithButton()
{
	uint8_t ledIndex = 0;
	uint8_t levelIndex = 0;
	setPinB(0, 1);
	setPinC(0, 1);
	while(1)
	{
		if(isPressedC() & (1 << BUTTON_PIN))
		{
			// Изключване на текущия светодиод.
			if(ledIndex < 8)
			{
				setPinB(ledIndex, 0);
			}
			else
			{
				setPinD(ledIndex - 8, 0);
			}

			++ledIndex;
			if(ledIndex > 15)
			{
				ledIndex = 0;

				// Изключване на текущото ниво.
				setPinC(levelIndex, 0);
				++levelIndex;
				if(levelIndex > 3)
				{
					levelIndex = 0;
				}

				// Включване на новото ниво.
				setPinC(levelIndex, 1);
			}

			// Включване на новия светодиод.
			if(ledIndex < 8)
			{
				setPinB(ledIndex, 1);
			}
			else
			{
				setPinD(ledIndex - 8, 1);
			}
		}
	}
}







/**
 * @brief Брояч за превключване между нивата.
 *
 * Сменя много бързо между нивата на кубчето, и така изглежда сякаш светят непрекъснато.
 */
ISR(TIMER0_OVF_vect)
{
	static volatile uint8_t level = 0;

	switch(level)
	{
		case 0:
			setPinC(2, 0);
			setPinC(0, 1);
			PORTB = LED[0];
			PORTD = LED[1];
			level = 1;
			break;
		case 1:
			setPinC(0, 0);
			setPinC(1, 1);
			PORTB = LED[2];
			PORTD = LED[3];
			level = 2;
			break;
		case 2:
			setPinC(1, 0);
			setPinC(3, 1);
			PORTB = LED[4];
			PORTD = LED[5];
			level = 3;
			break;
		case 3:
			setPinC(3, 0);
			setPinC(2, 1);
			PORTB = LED[6];
			PORTD = LED[7];
			level = 0;
			break;
	}
}

ISR(TIMER1_OVF_vect)
{
	if(shouldChangeMode())
	{
		ShouldChangeModeFlag = 1;
	}
}

/**
 * @brief Плоскост X=C
 */
void planeX()
{
	uint8_t x = 3;
	uint8_t up = 1;

	while(!ShouldChangeModeFlag)
	{
		clear();

		if(up)
		{
			++x;
			if(x > 3)
			{
				x = 2;
				up = 0;
			}
		}
		else
		{
			if(x == 0)
			{
				x = 1;
				up = 1;
			}
			else
			{
				--x;
			}
		}

		setLED(x, 0, 0, 1); setLED(x, 1, 0, 1); setLED(x, 2, 0, 1); setLED(x, 3, 0, 1);
		setLED(x, 0, 1, 1); setLED(x, 1, 1, 1); setLED(x, 2, 1, 1); setLED(x, 3, 1, 1);
		setLED(x, 0, 2, 1); setLED(x, 1, 2, 1); setLED(x, 2, 2, 1); setLED(x, 3, 2, 1);
		setLED(x, 0, 3, 1); setLED(x, 1, 3, 1); setLED(x, 2, 3, 1); setLED(x, 3, 3, 1);

		_delay_ms(100);
	}
}

/**
 * @brief Плоскост Y=C
 */
void planeY()
{
	uint8_t y = 3;
	uint8_t up = 1;

	while(!ShouldChangeModeFlag)
	{
		clear();

		if(up)
		{
			++y;
			if(y > 3)
			{
				y = 2;
				up = 0;
			}
		}
		else
		{
			if(y == 0)
			{
				y = 1;
				up = 1;
			}
			else
			{
				--y;
			}
		}

		setLED(0, y, 0, 1); setLED(1, y, 0, 1); setLED(2, y, 0, 1); setLED(3, y, 0, 1);
		setLED(0, y, 1, 1); setLED(1, y, 1, 1); setLED(2, y, 1, 1); setLED(3, y, 1, 1);
		setLED(0, y, 2, 1); setLED(1, y, 2, 1); setLED(2, y, 2, 1); setLED(3, y, 2, 1);
		setLED(0, y, 3, 1); setLED(1, y, 3, 1); setLED(2, y, 3, 1); setLED(3, y, 3, 1);

		_delay_ms(100);
	}
}

/**
 * @brief Плоскост Z=C
 */
void planeZ()
{
	uint8_t z = 3;
	uint8_t up = 1;

	while(!ShouldChangeModeFlag)
	{
		clear();

		if(up)
		{
			++z;
			if(z > 3)
			{
				z = 2;
				up = 0;
			}
		}
		else
		{
			if(z == 0)
			{
				z = 1;
				up = 1;
			}
			else
			{
				--z;
			}
		}

		setLED(0, 0, z, 1); setLED(1, 0, z, 1); setLED(2, 0, z, 1); setLED(3, 0, z, 1);
		setLED(0, 1, z, 1); setLED(1, 1, z, 1); setLED(2, 1, z, 1); setLED(3, 1, z, 1);
		setLED(0, 2, z, 1); setLED(1, 2, z, 1); setLED(2, 2, z, 1); setLED(3, 2, z, 1);
		setLED(0, 3, z, 1); setLED(1, 3, z, 1); setLED(2, 3, z, 1); setLED(3, 3, z, 1);

		_delay_ms(100);
	}
}

/**
 * @brief От един ъгъл до друг и обратно, по диагонал. За всеки ъгъл отдолу.
 */
void cornerToCorner()
{
	uint8_t sum = 0;
	uint8_t up = 1;
	uint8_t corner = 0;

	while(!ShouldChangeModeFlag)
	{
		clear();

		for(uint8_t x = 0; x <= sum; ++x)
		{
			for(uint8_t y = 0; y <= sum; ++y)
			{
				for(uint8_t z = 0; z <= sum; ++z)
				{
					if(x + y + z == sum)
					{
						switch(corner)
						{
							case 0:
								setLED(x, y, z, 1);
								break;
							case 1:
								setLED(3 - x, y, z, 1);
								break;
							case 2:
								setLED(3 - x, 3 - y, z, 1);
								break;
							case 3:
								setLED(x, 3 - y, z, 1);
								break;
						}
					}
				}
			}
		}

		if(up)
		{
			++sum;
			if(sum > 9)
			{
				sum = 8;
				up = 0;
			}
		}
		else
		{
			if(sum == 0)
			{
				++corner;
				if(corner > 3)
				{
					corner = 0;
				}
				up = 1;
			}
			else
			{
				--sum;
			}
		}

		_delay_ms(100);
	}
}

/**
 * @brief Преминаване от горе надолу и обратно, по едно, произволно, като после спират.
 */
void ledExchange()
{
	for(uint8_t x = 0; x < 4; ++x)
	{
		for(uint8_t y = 0; y < 4; ++y)
		{
			setLED(x, y, (rand() % 2 == 0 ? 0 : 3), 1);
		}
	}

	uint8_t moving = 0;
	uint8_t x = 0, y = 0, z = 0;
	uint8_t up = 0;

	while(!ShouldChangeModeFlag)
	{
		if(!moving)
		{
			x = rand() % 4;
			y = rand() % 4;
			z = getLED(x, y, 0) ? 0 : 3;
			up = z == 0;
			moving = 1;
		}
		else
		{
			setLED(x, y, z, 0);

			if(up)
			{
				++z;
				if(z == 3)
				{
					moving = 0;
				}
			}
			else
			{
				if(z == 0)
				{
					moving = 0;
				}
				else
				{
					--z;
				}
			}

			setLED(x, y, z, 1);
		}

		_delay_ms(100);
	}
}

/**
 * @brief Случайно светкане и гаснене.
 */
void randomSequence()
{
	while(!ShouldChangeModeFlag)
	{
		clear();

		setLED(rand() % 4, rand() % 4, rand() % 4, 1);

		_delay_ms(20);
	}
}

/**
 * @brief Имитация на дъжд. Падат от горе надолу.
 */
void rain()
{
	while(!ShouldChangeModeFlag)
	{
		// Светнатите се изместват с един надолу.
		for(uint8_t x = 0; x < 4; ++x)
		{
			for(uint8_t y = 0; y < 4; ++y)
			{
				for(uint8_t z = 0; z < 3; ++z)
				{
					setLED(x, y, z, getLED(x, y, z + 1));
				}
			}
		}

		// Изчистване на най-горния ред.
		for(uint8_t x = 0; x < 4; ++x)
		{
			for(uint8_t y = 0; y < 4; ++y)
			{
				setLED(x, y, 3, 0);
			}
		}

		// Светват произволен брой случайни светодиоди на най-горния ред.
		uint8_t count = rand() % 3;
		while(count)
		{
			setLED(rand() % 4, rand() % 4, 3, 1);
			--count;
		}

		_delay_ms(100);
	}
}

int main()
{
	// Порт B: анодите на светодиоди 0-3, 4-7.
	DDRB = 0xFF;
	// Порт D: анодите на светодиоди 8-11, 12-15.
	DDRD = 0xFF;
	// Порт C 0-3: катодите на нивата.
	DDRC = (1 << PINC0) | (1 << PINC1) | (1 << PINC2) | (1 << PINC3);
	// Порт C 4: бутон с включен вътрешен резистор към 5V.
	DDRC &= ~(1 << BUTTON_PIN);
	setPinC(BUTTON_PIN, 1);

	// Брояч за нивата.
	TCCR0B |= 1 << CS01;	// Делене на 8.
	TIMSK0 |= 1 << TOIE0;	// Включване на прекъсването при превъртане
	// Брояч за бутона.
	TCCR1B |= 1 << CS00;	// Без делене.
	TIMSK1 |= 1 << TOIE1;	// Включване на прекъсването при превъртане
	sei();

	PORTB = 0;
	PORTD = 0;

	const uint8_t modesCount = 7;
	uint8_t mode = eeprom_read_byte((uint8_t*)0);
	if(mode >= modesCount)
	{
		mode = 0;
	}

	while(1)
	{
		clear();

		switch(mode)
		{
			case 0:
				planeX();
				break;
			case 1:
				planeY();
				break;
			case 2:
				planeZ();
				break;
			case 3:
				cornerToCorner();
				break;
			case 4:
				ledExchange();
				break;
			case 5:
				randomSequence();
				break;
			case 6:
				rain();
				break;
		}

		// Връщане на стойността на флага към 0, за да може пак да се смени.
		ShouldChangeModeFlag = 0;

		++mode;
		if(mode >= modesCount)
		{
			mode = 0;
		}

		eeprom_write_byte((uint8_t*)0, mode);
	}

	return 0;
}
